% Rapport de projet - MixedQAP
% Kilian GUILLAUME; Raphaël CHARLES
% March 1, 2021

Travail produit par Kilian GUILLAUME et Raphaël CHARLES, dans le cadre du cours d'optimisation assuré par M. Verel ainsi que M. Chotard au sein du master I2L.

\tableofcontents
\newpage

# Présentation du sujet

Le problème traité dans ce rapport est une variante du Quadratic Assignment Problem, appelée ici Mixed-QAP.

Il s'agit de disposer *n* objets dans *n* emplacements en minimisant le produit du flux par la distance entre toutes les paires d'objets (principe du QAP classique), tout en assignant à ces objets une *activité* (ou un *poids*), dont la somme est égale à 1.

La fonction de flux entre deux objets est à la fois proportionnelle à l'activité du premier objet, et à celle du second.

# Optimisation combinatoire

## Hill-Climbing Best Improvement



### Pseudo-code pour Hill-Climbing Best Improvement

```python
s solution sur laquelle on joue apply
s’ solution initiale <- s
while le temps n’est pas écoulé
	ini_fit <- eval(s)
	for i index de la première valeur à permuter
		for j index de la seconde valeur à permuter
			tampon <- s’
			s’ <- permutation (i,j)
			if eval(s’) > eval(s)
				s <- s’
			s’ <- tampon
	if eval(s) == ini_fit
		break
	s’ <- s
```



## Tabu Search



### Pseudo-code pour Tabu Search

```python
s solution sur laquelle on joue apply
s’ solution initiale <- s
tabu map des permutations effectuées stockées avec un timer
while le temps n’est pas écoulé
	ini_fit <- eval(s)
	tabu <- tabu avec les timers décrémentés et les entrées au timer = 0 supprimées
	for i index de la première valeur à permuter
		for j index de la seconde valeur à permuter
			if tabu contient la permutation (i,j)
				continue
			tampon <- s’
			s’ <- permutation (i,j)
			if eval(s’) > eval(s)
				tabu <- tabu + ((i, j), timer)
				s <- s’
			s’ <- tampon
	if eval(s) == ini_fit
		break
	s’ <- s
```



## Implémentation de l'évaluation incrémentale du calcul de la fitness

Le but de l'évaluation incrémentale du calcul de la fitness est de réduire le temps d'évaluation d'une solution voisine (échange entre deux valeurs).

L'article cité dans le sujet nous donne pour cela la formule suivante :

```
total <- 0
Pour toute valeur de k allant de 0 à n-1 (r et s exclus)
    total <- total +
        (distance[sigma[s]][sigma[k]] - distance[sigma[r]][sigma[k]]) *
        (flow[s][k] - flow[s][k])
total <- total * 2
```

Malheureusement, lors de l'implémentation de cette formule, nous ne trouvons pas le meme résultat que lorsque nous faisons s.fitness - s'.fitness.

Il nous a donc été impossible d'utiliser cette formule incrémentale pour accélerer notre HillClimbingBest ou notre TabuSearch.

## Performances

Le graphe ci-dessous affiche la performance notée de 0 à 1 de chaque algorithme implémenté avec quatre temps d'exécution maximum différents, en fonction de la taille de l'instance testée.

À noter que le 1 correspond ici au meilleur score obtenu, tout algorithme confondu, et non pas le réel optimum.

Afin de limiter les effets du bruit dans les données récoltées, les courbes représentent les moyennes flottantes sur environ 10 valeurs pour chaque algorithme.

On remarque ainsi que l'algorithme qui performe le mieux est le Hill-Climbing Best-Improvement à qui l'on donne 10 secondes d'exécution. Il est également très stable dans la qualité de ses résultats.

Plus globalement, les algorithmes Tabu 10s, 5s et 1s ainsi que HC Best 5s et 1s donnent de bons résultats également, même s'ils perdent en stabilité lorsque la taille de l'instance dépasse 80.

![Graphique de comparaison des performances](graph.svg)

# Optimisation continue

Lors de l'application de l'algorithme (1+1)ES, on peut observer une variation du résultat en fonction de l'ordre dans lequel nous avons optimisé les différents xi. Cela implique qu'il est potentiellement nécessaire de tester les n! combinaisons possibles afin de trouver la meilleure.
