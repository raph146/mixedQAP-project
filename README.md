# MixedQAP Project

Authors: 
  Sébastien Verel, Kilian GUILLAUME, Raphaël CHARLES

Date: 
  2021/02/11 - Version 1.1


## Description:

Please see the project description at: 
https://www.overleaf.com/read/phjcbfrmfycr


## Instance files:

The directory instances/ contains the set of mixed-variable Quadratic Assignment Problem (mixed-QAP) instance files.


## Code:

We worked only with java for this project, so everything code related made by ourselves is located in the code/src/java directory.

To compile and run the code, we would recommand the use of an IDE, for example Eclipse or IntelliJ, as we used packages and therefore it may be harder to compile and run via command line.

Test files are located in the code/src/java/tests directory and can each be ran separately from the others.

It should be possible to compute every instance with every algorithm by running the Benchmark.java file, although it may very well take a large amount of time.
Once done, and after checking that the requirements are met, it should be possible to run the generate_graph.py file to generate a graph showing each algorithm's performance compared to others.
