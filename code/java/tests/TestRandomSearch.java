/**
 * 
 */

import algo.RandomSearch;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.Random;

/**
 * @author verel
 *

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
  version 0 : 2020/11/24

 */
public class TestRandomSearch {
    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
    	String instanceFileName = "instances/mixedQAP_uni_6_-100_100_1.dat";

    	MixedQAPeval eval = new MixedQAPeval(instanceFileName);

	    Solution solution = new Solution(6);

		for(int i = 0; i < 6; i++) {
			solution.x[i] = 1.0 / 6;
			solution.sigma[i] = i;
		}
		eval.apply(solution);

		Random rnd = new Random();
		rnd.setSeed(1337);
		rnd.setSeed(1);
		
		RandomSearch search = new RandomSearch(Duration.ofSeconds(5), eval, rnd);

		System.out.println(solution);
		search.apply(solution);
		System.out.println(solution);

      System.out.println();

    }


}
