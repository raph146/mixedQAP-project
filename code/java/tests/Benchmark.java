/**
 *
 */

import algo.HillClimbingBest;
import algo.RandomSearch;
import algo.Search;
import algo.TabuSearch;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Benchmark {
    /**
     * @param args
     */
    public static void main(String[] args) throws IOException, ReflectiveOperationException {
        /*
        ** Output file structure: **
        the number of classes
        name of class 1
        name of class 2
        …
        the number of configurations
        duration of config 1
        duration of config 2
        …
        instance 1 name
        class 1 configuration 1 score duration
        class 1 configuration 2 score duration
        …
        class 2 configuration 1 score duration
        …
        instance 2 name
        …
        [end of file]
         */
        FileWriter outputWriter = new FileWriter("output.txt");

        List<Class<? extends Search>> classes = new ArrayList<>();
        classes.add(HillClimbingBest.class);
        classes.add(RandomSearch.class);
        classes.add(TabuSearch.class);

        outputWriter.write(classes.size() + "\n");
        for (Class<? extends Search> searchClass : classes)
            outputWriter.write(searchClass.getCanonicalName() + "\n");
        
        // The multiple test configurations
        int testConfigurations = 4;
        Duration[] durations = new Duration[]{Duration.ofMillis(100), Duration.ofSeconds(1), Duration.ofSeconds(5), Duration.ofSeconds(10)};
        int[] iterations = new int[]{20, 5, 2, 1};

        outputWriter.write(testConfigurations + "\n");
        for (int i=0; i < testConfigurations; i++)
            outputWriter.write(durations[i].toSeconds() + "\n");
        
        for (File instance_file : (new File("instances")).listFiles()) {
            outputWriter.write(instance_file.getPath() + "\n");
            System.out.println(instance_file);
            MixedQAPeval eval = new MixedQAPeval(instance_file.getAbsolutePath());

            for (Class<? extends Search> searchClass : classes) {
                System.out.println("\t" + searchClass.getCanonicalName());

                for (int confIndex = 0; confIndex < testConfigurations; confIndex++) {
                    Duration confDuration = durations[confIndex];
                    int iters = iterations[confIndex];
                    if (!searchClass.getField("isRandom").getBoolean(null))
                        iters = 1;
                    double fitness = 0;
                    double duration = 0;
                    
                    for (int iter=0; iter < iters; iter++) {
                        Solution solution = new Solution(eval.n);
                        for (int i = 0; i < eval.n; i++) {
                            solution.x[i] = 1.0 / eval.n;
                            solution.sigma[i] = i;
                        }
                        eval.apply(solution);

                        Constructor c = searchClass.getConstructor(new Class[]{Duration.class, MixedQAPeval.class});
                        Search s = (Search) c.newInstance(confDuration, eval);

                        long start = System.currentTimeMillis();
                        s.apply(solution);
                        duration += System.currentTimeMillis() - start;
                        fitness += solution.fitness;
                    }
                    if (iters > 1)
                        System.out.println("\t\t" + fitness/iters + " in " + duration/iters/1000. + "s (" + iters + " iterations)");
                    else
                        System.out.println("\t\t" + fitness/iters + " in " + duration/iters/1000 + "s");
                    outputWriter.write(fitness/iters + " " + duration/iters + "\n");
                }
            }
            outputWriter.flush();
        }
        outputWriter.close();
    }
}