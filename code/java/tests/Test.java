/**
 * 
 */
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.*;

/**
 * @author verel
 *

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
  version 0 : 2020/11/24

 */
public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
    	String instanceFileName = "instances/mixedQAP_uni_6_-100_100_1.dat";

    	MixedQAPeval eval = new MixedQAPeval(instanceFileName);

	    System.out.println(eval);

	    Solution solution = new Solution(6);

	    for(int i = 0; i < 6; i++) {
    	    solution.x[i] = 1.0 / 6;
        	solution.sigma[i] = i;
    	}

    	eval.apply(solution);

    	System.out.println(solution);

      System.out.println();

    	solution.printOnFlow();

    }


}
