/**
 *
 */

import algo.OnePlusOneES;
import algo.OnePlusOneESShuffled;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.FileNotFoundException;
import java.time.Duration;

//import evaluation.Solution;

/**
 * @author verel
 *

Author:
Sebastien Verel,
Univ. du Littoral Côte d'Opale, France.
version 0 : 2020/11/24

 */
public class TestOnePlusOneES {

    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
        String instanceFileName = "instances/mixedQAP_uni_20_-100_100_1.dat";

        MixedQAPeval eval = new MixedQAPeval(instanceFileName);

        Solution solution = new Solution(eval.n);

        for(int i = 0; i < eval.n; i++) {
            solution.x[i] = 1.0 / eval.n;
            solution.sigma[i] = i;
        }
        eval.apply(solution);

        OnePlusOneES search = new OnePlusOneES(Duration.ZERO, eval);

        System.out.println("base\t" + solution);
        search.apply(solution);
        System.out.println("ordered\t" + solution);

        for (int _i=0; _i<10; _i++) {
            solution = new Solution(eval.n);

            for(int i = 0; i < eval.n; i++) {
                solution.x[i] = 1.0 / eval.n;
                solution.sigma[i] = i;
            }
            eval.apply(solution);
            OnePlusOneESShuffled shuffled = new OnePlusOneESShuffled(Duration.ZERO, eval);
            shuffled.apply(solution);
            System.out.println("random\t" + solution);
        }

        System.out.println();

    }


}
