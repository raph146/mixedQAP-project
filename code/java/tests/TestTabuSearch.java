/**
 *
 */

import algo.TabuSearch;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.FileNotFoundException;
import java.time.Duration;

//import evaluation.Solution;

/**
 * @author verel
 *

Author:
Sebastien Verel,
Univ. du Littoral Côte d'Opale, France.
version 0 : 2020/11/24

 */
public class TestTabuSearch {

    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
        String instanceFileName = "instances/mixedQAP_uni_6_-100_100_1.dat";

        MixedQAPeval eval = new MixedQAPeval(instanceFileName);

        Solution solution = new Solution(6);

        for(int i = 0; i < 6; i++) {
            solution.x[i] = 1.0 / 6;
            solution.sigma[i] = i;
        }
        eval.apply(solution);

        TabuSearch search = new TabuSearch(Duration.ofSeconds(5), eval);

        System.out.println(solution);
        search.apply(solution);
        System.out.println(solution);

        System.out.println();

    }


}
