package operator;

import evaluation.Solution;

import java.util.Random;

public class RandomPermutation extends Operator {
    protected Random rand;
    protected int size;

    public RandomPermutation(Random rand, int size) {
        this.rand = rand;
        this.size = size;
    }
    @Override
    public void apply(Solution solution) {

        for (int i=0; i < solution.sigma.length; i++) {
            int randomPosition = rand.nextInt(solution.sigma.length);
            int temp = solution.sigma[i];
            solution.sigma[i] = solution.sigma[randomPosition];
            solution.sigma[randomPosition] = temp;
        }
    }
}