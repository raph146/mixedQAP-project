package operator;

import evaluation.Solution;

public abstract class Operator {
    public abstract void apply(Solution s);
}
