package operator;

import evaluation.Solution;

public class UniformContinue extends Operator {
    protected int n;
    public UniformContinue(int n) {
        this.n = n;
    }

    @Override
    public void apply(Solution solution) {
        solution.x = new double[n];
        for(int i = 0; i < n; i++)
            solution.x[i] = 1.0 / n;
        solution.modifiedX = true;
    }
}
