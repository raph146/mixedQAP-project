package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;
import operator.RandomPermutation;

import java.time.Duration;
import java.util.Random;

public class OnePlusOneES extends Search {
    public static boolean isRandom = true;

    protected MixedQAPeval eval;
    protected Random rand;

    public OnePlusOneES(Duration limit, MixedQAPeval eval) {
        super(limit);
        this.eval = eval;
        Random rnd = new Random();
        this.rand = rnd;
    }

    public OnePlusOneES(Duration limit, MixedQAPeval eval, Random rand) {
        super(limit);
        this.eval = eval;
        this.rand = rand;
    }

    public void apply_constraints(Solution solution) {
        // Negative values are already handled
        // Sum is equal to zero
        double total = 0;
        for (int i = 0; i < eval.n; i++) {
            total += solution.x[i];
        }
        if (total == 0) {
            for (int i = 0; i < eval.n; i++) {
                solution.x[i] = 1. / eval.n;
            }
        }
        // Sum is not one
        total = 0;
        for (int i = 0; i < eval.n; i++) {
            total += solution.x[i];
        }
        if (total != 1) {
            for (int i = 0; i < eval.n; i++) {
                solution.x[i] = solution.x[i] / total;
            }
        }
    }
    
    public void one_plus_one(int i, Solution _solution) {
        // A circular buffer containing the last 200 iters
        double[] last_200_results = new double[200];
        int result_index = 0;
        boolean has_gone_through_first_200 = false;
        double sigma = 0.1;
        double gamma = 1.1;
        while (true) {
            if (has_gone_through_first_200 && last_200_results[(result_index+1)%200] - _solution.fitness < 1E-6) {
                break;
            }
            // New value for x
            double xp = _solution.x[i] + rand.nextGaussian() * sigma;
            if (xp < 0) {
                xp = -xp;
            }
            Solution temp_sol = new Solution(_solution);
            temp_sol.x[i] = xp;
            apply_constraints(temp_sol);
            temp_sol.modifiedX = true;
            eval.apply(temp_sol);

            if (temp_sol.fitness > _solution.fitness) {
                _solution.copy(temp_sol);
                sigma = sigma * gamma;
            } else {
                sigma = sigma * Math.pow(gamma, -1./4.);
            }
            result_index++;
            if (result_index >= 200) {
                result_index = 0;
                has_gone_through_first_200 = true;
            }
            last_200_results[result_index] = _solution.fitness;
        }
    }
    
    @Override
    public void apply(Solution _solution) {
        for (int i=0; i < eval.n; i++) {
            one_plus_one(i, _solution);
        }
    }
}
