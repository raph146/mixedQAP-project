package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.time.Duration;
import java.util.Random;

public class OnePlusOneESShuffled extends OnePlusOneES {
    public OnePlusOneESShuffled(Duration limit, MixedQAPeval eval) {
        super(limit, eval);
    }

    public OnePlusOneESShuffled(Duration limit, MixedQAPeval eval, Random rand) {
        super(limit, eval, rand);
    }

    @Override
    public void apply(Solution _solution) {
        int order[] = new int[eval.n];
        for (int i=0; i < eval.n; i++) {
            order[i] = i;
        }
        for (int i=0; i < eval.n; i++) {
            int randomPosition = rand.nextInt(eval.n);
            int temp = order[i];
            order[i] = order[randomPosition];
            order[randomPosition] = temp;
        }
        for (int i=0; i < eval.n; i++) {
            one_plus_one(order[i], _solution);
        }
    }
}
