package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;
import operator.RandomPermutation;
import operator.UniformContinue;

import java.time.Duration;
import java.util.Date;
import java.util.Random;

public class RandomSearch extends Search {
    public static boolean isRandom = true;

    protected MixedQAPeval eval;
    protected Random rand;

    public RandomSearch(Duration limit, MixedQAPeval eval) {
        super(limit);
        this.eval = eval;
        Random rnd = new Random();
        this.rand = rnd;
    }
    
    public RandomSearch(Duration limit, MixedQAPeval eval, Random rand) {
        super(limit);
        this.eval = eval;
        this.rand = rand;
    }

    @Override
    public void apply(Solution _solution) {
        RandomPermutation random = new RandomPermutation(this.rand, _solution.sigma.length);
        Solution s = new Solution(_solution);
        long start = System.currentTimeMillis();
        while(System.currentTimeMillis() - start < this.limit.toMillis()) {
            random.apply(s);
            this.eval.apply(s);
            if (s.fitness > _solution.fitness)
                _solution.copy(s);
        }
    }
}
