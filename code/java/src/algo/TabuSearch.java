package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TabuSearch extends Search {
    protected MixedQAPeval eval;

    public TabuSearch(Duration limit, MixedQAPeval eval) {
        super(limit);
        this.eval = eval;
    }

    private static Integer decrement(Map.Entry<Integer, Integer> key, Integer value){
        return value--;
    }

    /**
     * Doesn't output the real delta
     */
    private double incrementalScore(MixedQAPeval eval, Solution sol, int r, int s) {
        double total = 0;
        for (int k = 0; k < eval.n; k++) {
            if (k == r || k == s) {
                continue;
            }
            // ask - ark
            double dist = eval.distance[sol.sigma[s]][sol.sigma[k]] - eval.distance[sol.sigma[r]][sol.sigma[k]];
            // bsk - brk
            double flow = sol.flow[s][k] - sol.flow[r][k];
            total += dist * flow;
        }
        return total * 2;
    }

    /**
     * Outputs the real delta
     */
    /*
    private double unoptimizedScore(MixedQAPeval eval, Solution sol, Solution sol2) {
        double total = 0;
        for (int i=0; i < eval.n; i++) {
            for (int j=0; j < eval.n; j++) {
                total += eval.distance[sol.sigma[i]][sol.sigma[j]] * sol.flow[i][j] - eval.distance[sol2.sigma[i]][sol2.sigma[j]] * sol.flow[i][j];
            }
        }
        return total;
    }
    */
    
    @Override
    public void apply(Solution _solution) {
        Solution s = new Solution(_solution);
        Map<Map.Entry<Integer, Integer>, Integer> tabu = new HashMap<>();
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < this.limit.toMillis()) {
            double start_fitness = _solution.fitness;
            tabu.replaceAll(TabuSearch::decrement);
            tabu.entrySet()
                    .stream()
                    .filter(map -> map.getValue() > 0)
                    .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
            for (int i = 1; i < s.sigma.length; i++) {
                for (int j = 0; j < i; j++) {
                    if (tabu.containsKey(new AbstractMap.SimpleEntry<>(i, j))) {
                        continue;
                    }
                    int temp = s.sigma[i];
                    s.sigma[i] = s.sigma[j];
                    s.sigma[j] = temp;
                    this.eval.apply(s);

                    if (s.fitness > _solution.fitness) {
                        tabu.put(new AbstractMap.SimpleEntry<>(i, j), s.sigma.length);
                        _solution.copy(s);
                    }

                    temp = s.sigma[i];
                    s.sigma[i] = s.sigma[j];
                    s.sigma[j] = temp;
                    if (System.currentTimeMillis() - start > this.limit.toMillis())
                        break;
                }
            }
            if (_solution.fitness == start_fitness)
                break;
            s.copy(_solution);
        }
    }
}
