package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.time.Duration;

public abstract class Search {
    public static boolean isRandom = false;
    public Search(Duration d, MixedQAPeval e) {}
    protected Duration limit;
    public Search(Duration limit) {
        this.limit = limit;
    }
    public abstract void apply(Solution s);
}
