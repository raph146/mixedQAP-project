package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.time.Duration;

public class HillClimbingBest extends Search {
    protected MixedQAPeval eval;

    public HillClimbingBest(Duration limit, MixedQAPeval eval) {
        super(limit);
        this.eval = eval;
    }

    @Override
    public void apply(Solution _solution) {
        Solution s = new Solution(_solution);
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < this.limit.toMillis()) {
            double start_fitness = _solution.fitness;
            for (int i = 1; i < s.sigma.length; i++) {
                for (int j = 0; j < i; j++) {
                    int temp = s.sigma[i];
                    s.sigma[i] = s.sigma[j];
                    s.sigma[j] = temp;
                    this.eval.apply(s);

                    if (s.fitness > _solution.fitness)
                        _solution.copy(s);

                    temp = s.sigma[i];
                    s.sigma[i] = s.sigma[j];
                    s.sigma[j] = temp;
                    if (System.currentTimeMillis() - start > this.limit.toMillis())
                        break;
                }
            }
            if (_solution.fitness == start_fitness)
                break;
            s.copy(_solution);
        }
    }
}
