import re
from typing import Dict, Tuple

import matplotlib
import matplotlib.pyplot as plt
from scipy.ndimage.filters import uniform_filter1d


Configuration = Tuple[str, float]
Instances = Dict[str, Dict[Configuration, float]]

SHORT_ALGO_NAMES = {
    "HillClimbingBest": "HC Best",
    "RandomSearch": "Random",
    "TabuSearch": "Tabu"
}

def load_output(path: str) -> Instances:
    with open(path, "r") as output:
        algo_count = int(output.readline())
        algos = []
        for i in range(algo_count):
            algos.append(output.readline())
        duration_count = int(output.readline())
        durations = []
        for i in range(duration_count):
            durations.append(float(output.readline()))
        instances = {}
        while True:
            instance_name = output.readline()
            if not instance_name:
                break
            scores = {}
            for algo in algos:
                for duration in durations:
                    fitness, duration_taken = output.readline().split(" ")
                    # TODO how to use duration_taken?
                    scores[(algo, duration)] = float(fitness)
            instances[instance_name] = scores
    return instances

def normalize_per_instance(instances: Instances) -> None:
    """Normalize in-place the score of all configurations for all instances."""
    for instance_name, scores in instances.items():
        max_score = max(scores.values())
        for configuration in list(scores.keys()):
            scores[configuration] /= max_score

def size_from_name(instance_name: str) -> int:
    """Return the size of an instance from it's name."""
    match = re.search(r"mixedQAP_uni_(?P<size>\d+)_-?\d+_\d+_1\.dat", instance_name)
    if match is None:
        raise ValueError("Couldn't extract size from given name.")
    return int(match["size"])

def order_by_size(instances: Instances) -> Instances:
    return dict(sorted(instances.items(), key=lambda pair: size_from_name(pair[0])))

def generate_graph(instances: Instances, output_path: str) -> str:
    """Generate a graph for the instances to the output_path."""
    configurations = list(instances.values())[0].keys()
    sizes = [size_from_name(name) for name in instances.keys()]
    fig, ax = plt.subplots()

    for configuration in configurations:
        values = [scores[configuration] for scores in instances.values()]
        # Floating average of 10 values, to make the graph at least a bit readable
        values = uniform_filter1d(values, 10)
        algo = configuration[0].removeprefix("algo.").rstrip("\n")
        duration = configuration[1]
        ax.plot(sizes, values, label=f"{SHORT_ALGO_NAMES[algo]} {duration}s")

    ax.set_ylabel("Performance")
    ax.set_title("Performance en fonction de la taille de l'instance")
    ax.legend(title="Configuration", fontsize="small")

    # Add margin to the left to make legend not cover anything
    ax.set_xbound(-10, None)

    fig.savefig(output_path)
    print(f"Figure saved as {output_path!r}")


def main() -> None:
    datapoints = load_output("output_rendu_1.txt")
    normalize_per_instance(datapoints)
    datapoints = order_by_size(datapoints)
    generate_graph(datapoints, "graph.svg")

if __name__ == "__main__":
    main()
